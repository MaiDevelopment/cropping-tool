#include "stdafx.h"
#include "windows.h"
#include <vector>
#include <string.h>
using namespace std;


#define BYTE unsigned char
#define PI 3.1415926535897932384626433832795
#define DIF_LEVEL 6
#define NAME_LENGTH 255

// Global variables
int
	gnWidth,
	gnHeight,
	
	gnDifLevel;

BYTE
	*gpBufIn = NULL,
	*gpBufOut = NULL,
	*gpMask = NULL;

#define SAFE_DELETE(x) if(x) {delete x; x = NULL;}

bool ReadInputFile(char *sFile);
void CalculateHistogram();
void CreateMask();
void FilterMask();
void ChangeBackground();
bool SaveResultToBMP(char *sFile);
bool SaveMaskToBin(char *sFile);
bool SaveResultToJPG(char* sFile);
void CleanUp();

int main(int argc, char* argv[])
{
	// Should be run with 1 parameter - source jpg file
	if (argc != 2)
	{
		printf("Run as \"SimpleEdge\" <filename.jpg>");
		return -1;
	}

	// Define input and output file names
	char
		sOnlyName[NAME_LENGTH],
		sInFileName[NAME_LENGTH],
		sHistFileName[NAME_LENGTH],
		sOutFileName[NAME_LENGTH],
		sBmpFileName[NAME_LENGTH],
		sMskFileName[NAME_LENGTH];

	strcpy(sInFileName, (const char*) argv[1]);

	// Hardcode file name for simplicity
//	sprintf(sInFileName, "d:\\example7.jpg", sOnlyName);

	strncpy(sOnlyName, sInFileName, strlen(sInFileName) - 4);
	sOnlyName[strlen(sInFileName) - 4] = '\x0';
	sprintf(sOutFileName, "%s_out.jpg", sOnlyName);
	sprintf(sHistFileName, "%s.txt", sOnlyName);
	sprintf(sBmpFileName, "%s_out.bmp", sOnlyName);
	sprintf(sMskFileName, "%s_out.bin", sOnlyName);

	// Read the input file, 
	// Put image dimension into global variables gnWidth and gnHeight,
	// put pixel values into global variable gpBufIn 
	if (!ReadInputFile(sInFileName))
	{
		printf("ERROR: cannot read file %s", sInFileName);
		CleanUp();
		return - 1;
	}

	// Calculate source image histogram to define optimal difference level
	// between the item and the background
	CalculateHistogram();

	// Separate the item by finding background mask
	CreateMask();

	// Perform some filtering to avoid noise mask dots
	FilterMask();

	// Change background color to required value
	ChangeBackground();

	// Save result to BMP file
	// Preferred, as it performs no lossy compression
	if (!SaveResultToBMP(sBmpFileName))
	{
		printf("ERROR: cannot write file %s", sBmpFileName);
	}

	// Save result to JPG file
	if (!SaveResultToJPG(sOutFileName))
	{
		printf("ERROR: cannot write file %s", sOutFileName);
	}

	// Save mask matrix to a binary file
	if (!SaveMaskToBin(sMskFileName))
	{
		printf("ERROR: cannot write file %s", sMskFileName);
	}

	CleanUp();
	return 0;
}


// Use jpeglib to read input jpeg file
// All pixels values are loaded to gpBufIn
bool ReadInputFile(char *sFile)
{

	// Initialize jpeglib stuff
	jpeg_decompress_struct
		dinfo;
	jpeg_error_mgr 
		jerr;
	dinfo.err = jpeg_std_error(&jerr);
	jpeg_create_decompress(&dinfo);

	FILE 
		*infile;
     if ((infile = fopen(sFile, "rb")) == NULL)
		return false;

	jpeg_stdio_src(&dinfo, infile);

	jpeg_read_header(&dinfo, TRUE);

	// Get the image dimensions
	gnWidth = dinfo.image_width;
	gnHeight = dinfo.image_height;

	// Create input and output buffers
	SAFE_DELETE(gpBufIn);
	gpBufIn = new BYTE[gnWidth * gnHeight * 3];
	SAFE_DELETE(gpBufOut);
	gpBufOut = new BYTE[gnWidth * gnHeight * 3];

	JSAMPROW
		pRow[1];
	pRow[0] = gpBufIn;

	if (!jpeg_start_decompress(&dinfo))
		return false;

	// Read lines to the buffer
	for (int j = 0; j < gnHeight; j++)
	{
		pRow[0] = gpBufIn + (gnWidth * 3) * j;
		jpeg_read_scanlines(&dinfo, pRow, 1);
	}

	jpeg_finish_decompress(&dinfo);

	jpeg_destroy_decompress(&dinfo);

	fclose(infile);

	// copy source image to destination buffer
	memcpy(gpBufOut, gpBufIn, gnWidth * gnHeight * 3);

	return true;
}

// Get the image histogram to find the optimal difference value between item and background
// Work good for solid items, worse for item with some painting
// Calculating integral histogram as grayscale (R + G + B) / 3
void CalculateHistogram()
{

	int
		*pHist = new int[256];

	memset(pHist, 0, 256 * sizeof(int));

	for (int i = 0; i < gnWidth * gnHeight; i++)
	{
		pHist[(gpBufIn[i * 3 + 0] + gpBufIn[i * 3 + 1] + gpBufIn[i * 3 + 2]) / 3]++;
	}


	__int64 
		sum = 0, 
		num = 0, 
		mean = 0;
	double
		stddev = 0;
	for (int i = 0; i < 256; i++)
	{
		sum += __int64(pHist[i]) * __int64(i);
		num += __int64(pHist[i]);
	}

	if (num)
	{
		mean = sum / num;
	}
	else
	{
		mean = 0;
	}

	for (int j = 0; j < 256; j++)
	{
		stddev += double(pHist[j] * (mean - j) * (mean - j));
	}

	if (num)
	{
		stddev = sqrt(stddev / double(num));
//		gnDifLevel = int(stddev / 5);
		gnDifLevel = DIF_LEVEL;
	}
	else
		gnDifLevel = DIF_LEVEL;


/*
	// Save histogram to file if necessary
	char
		*pText = new char[1024];
	FILE
		*pHistFile;
	pHistFile = fopen("d:\\hist.txt", "wb");
	if (pHistFile == NULL)
		return;

	for (int i = 0; i < 256; i++)
	{
		sprintf(pText, "%d\r\n", pHist[i]);
		fwrite(pText, sizeof(char), strlen(pText),pHistFile);
	}
	delete pText;
	fclose(pHistFile);
*/

	delete pHist;
}

// Fills gpMask buffer with the mask matrix values
// Analyze each pair of pixels to decide if it is the item edge
void CreateMask()
{
	SAFE_DELETE(gpMask);
	gpMask = new BYTE[gnWidth * gnHeight];
	memset(gpMask, 0, gnWidth * gnHeight);

	vector <int>
		vPixel,
		vNewSet;

	// Starting from 2 initial pixels: top-left and top-right
	vPixel.push_back(0);
	gpMask[0] = 1;
	vPixel.push_back(gnWidth - 1);
	gpMask[gnWidth - 1] = 1;

	bool
		bWas = TRUE;
	int
		nDif = gnDifLevel;

	while (bWas)
	{
		// bWas is set to TRUE when no new background pixels are available
		bWas = FALSE;

		// Running through all new background pixels to analyze its neighbours
		// If difference more then nDif, this is an item pixels
		// If difference less then nDif, this is background pixels, and it should be analyzed in next iteration
		for (unsigned int i = 0; i < vPixel.size(); i++)
		{
			// All pixels except left side
			if (vPixel[i] > 0)
			{
				if (gpMask[vPixel[i] - 1] == 0)
				{
					if ((abs(gpBufIn[(vPixel[i] - 1) * 3 + 0] - gpBufIn[(vPixel[i]) * 3 + 0]) +
						abs(gpBufIn[(vPixel[i] - 1) * 3 + 1] - gpBufIn[(vPixel[i]) * 3 + 1]) +
						abs(gpBufIn[(vPixel[i] - 1) * 3 + 2] - gpBufIn[(vPixel[i]) * 3 + 2])) < nDif)
					{
						gpMask[vPixel[i] - 1] = 1;
						vNewSet.push_back(vPixel[i] - 1);
						bWas = TRUE;
					}
					else
					{
						gpMask[vPixel[i] - 1] = 2;
					}
				}
			}

			// All pixels except top line
			if (vPixel[i] >= gnWidth)
			{
				if (gpMask[vPixel[i] - gnWidth] == 0)
				{
					if ((abs(gpBufIn[(vPixel[i] - gnWidth) * 3 + 0] - gpBufIn[(vPixel[i]) * 3 + 0]) +
						abs(gpBufIn[(vPixel[i] - gnWidth) * 3 + 1] - gpBufIn[(vPixel[i]) * 3 + 1]) +
						abs(gpBufIn[(vPixel[i] - gnWidth) * 3 + 2] - gpBufIn[(vPixel[i]) * 3 + 2])) < nDif)
					{
						gpMask[vPixel[i] - gnWidth] = 1;
						vNewSet.push_back(vPixel[i] - gnWidth);
						bWas = TRUE;
					}
					else
					{
						gpMask[vPixel[i] - gnWidth] = 2;
					}
				}
			}

			// All pixels except bottom line
			if (vPixel[i] < gnWidth * gnHeight - 1)
			{
				if (gpMask[vPixel[i] + 1] == 0)
				{
					if ((abs(gpBufIn[(vPixel[i] + 1) * 3 + 0] - gpBufIn[(vPixel[i]) * 3 + 0]) +
						abs(gpBufIn[(vPixel[i] + 1) * 3 + 1] - gpBufIn[(vPixel[i]) * 3 + 1]) +
						abs(gpBufIn[(vPixel[i] + 1) * 3 + 2] - gpBufIn[(vPixel[i]) * 3 + 2])) < nDif)
					{
						gpMask[vPixel[i] + 1] = 1;
						vNewSet.push_back(vPixel[i] + 1);
						bWas = TRUE;
					}
					else
					{
						gpMask[vPixel[i] + 1] = 2;
					}
				}

			}

			// All pixels except right side
			if (vPixel[i] < gnWidth * (gnHeight - 1) - 1)
			{
				if (gpMask[vPixel[i] + gnWidth] == 0)
				{
					if ((abs(gpBufIn[(vPixel[i] + gnWidth) * 3 + 0] - gpBufIn[(vPixel[i]) * 3 + 0]) +
						abs(gpBufIn[(vPixel[i] + gnWidth) * 3 + 1] - gpBufIn[(vPixel[i]) * 3 + 1]) +
						abs(gpBufIn[(vPixel[i] + gnWidth) * 3 + 2] - gpBufIn[(vPixel[i]) * 3 + 2])) < nDif)
					{
						gpMask[vPixel[i] + gnWidth] = 1;
						vNewSet.push_back(vPixel[i] + gnWidth);
						bWas = TRUE;
					}
					else
					{
						gpMask[vPixel[i] + gnWidth] = 2;
					}
				}

			}
		}

		// New pixel set to analyze
		vPixel = vNewSet;
		vNewSet.clear();
	}

}

// Filtering the mask matrix in gpMask to eliminate "noise pixels"
// the rule is: if item pixel has 5 or more neighbours which are background,
// then the pixel is also background
void FilterMask()
{
	bool
		bWas = TRUE;
	while(bWas)
	{
		bWas = FALSE;

		for (int j = 1; j < gnHeight - 1; j++)
		{
			for (int i = 1; i < gnWidth - 1; i++)
			{
				int
					num = 0;

				if (gpMask[j * gnWidth + i] == 2)
				{
					if (gpMask[(j - 1) * gnWidth + i] == 1)
						num++;
					if (gpMask[(j + 1) * gnWidth + i] == 1)
						num++;
					if (gpMask[j * gnWidth + i + 1] == 1)
						num++;
					if (gpMask[j * gnWidth + i - 1] == 1)
						num++;
					if (gpMask[(j - 1) * gnWidth + i - 1] == 1)
						num++;
					if (gpMask[(j + 1) * gnWidth + i - 1] == 1)
						num++;
					if (gpMask[(j + 1) * gnWidth + i + 1] == 1)
						num++;
					if (gpMask[(j - 1) * gnWidth + i + 1] == 1)
						num++;

					if (num >= 5)
					{
						gpMask[j * gnWidth + i] = 1;
						bWas = TRUE;
					}

				}
			}
		}
	}

}

// This function use mask data from gpMask to change background 
// to any value. It can be 0 (black), 255 (white), or some another rule to generate any pattern
void ChangeBackground()
{
	for (int j = 0; j < gnHeight; j++)
	{
		for (int i = 0; i < gnWidth; i++)
		{

			if (gpMask[j * gnWidth + i] == 1)
			{
				// Filling background with black
				gpBufOut[(j * gnWidth + i) * 3 + 0] = 0;
				gpBufOut[(j * gnWidth + i) * 3 + 1] = 0;
				gpBufOut[(j * gnWidth + i) * 3 + 2] = 0;
/*
				// Filling background with ramp pattern
				pBufOut[(j * nWidth + i) * 3 + 0] = (i + j) * 4;
				pBufOut[(j * nWidth + i) * 3 + 1] = (i + j) * 4;
				pBufOut[(j * nWidth + i) * 3 + 2] = (i + j) * 4;
*/
			}

			// If the item pixels contain zero value, 
			// inc them by 1 to avoid misunderstanging on future steps

			if (gpMask[j * gnWidth + i] == 0)
			{
				if (gpBufOut[(j * gnWidth + i) * 3 + 0] == 0)
					gpBufOut[(j * gnWidth + i) * 3 + 0] = 1;
				if (gpBufOut[(j * gnWidth + i) * 3 + 1] == 0)
					gpBufOut[(j * gnWidth + i) * 3 + 1] = 1;
				if (gpBufOut[(j * gnWidth + i) * 3 + 2] == 0)
					gpBufOut[(j * gnWidth + i) * 3 + 2] = 1;

			}

		}
	}

}

// Writes down all pixels from gpBufOut to the destination
// ordinary bitmap file named sFile
bool SaveResultToBMP(char *sFile)
{
	// File header
	BITMAPFILEHEADER
		bmpHeader;
	bmpHeader.bfType = 19778; // BM
	bmpHeader.bfSize = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFO) + gnWidth * gnHeight * 3;
	bmpHeader.bfReserved1 = 0;
	bmpHeader.bfReserved2 = 0;
	bmpHeader.bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFO);

	// Image iformation
	BITMAPINFO
		bmpInfo;
	bmpInfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bmpInfo.bmiHeader.biWidth = gnWidth;
	bmpInfo.bmiHeader.biHeight = - gnHeight;
	bmpInfo.bmiHeader.biPlanes = 1;
	bmpInfo.bmiHeader.biBitCount = 24;
	bmpInfo.bmiHeader.biCompression = BI_RGB;
	bmpInfo.bmiHeader.biSizeImage = 0;
	bmpInfo.bmiHeader.biXPelsPerMeter = 0;
	bmpInfo.bmiHeader.biYPelsPerMeter = 0;
	bmpInfo.bmiHeader.biClrUsed = 0;
	bmpInfo.bmiHeader.biClrImportant = 0;

	FILE 
		*bmpFile;
     if ((bmpFile = fopen(sFile, "wb")) == NULL)
	{
		return false;
    }

	fwrite(&bmpHeader, sizeof(BITMAPFILEHEADER), 1, bmpFile);
	fwrite(&bmpInfo, sizeof(BITMAPINFO), 1, bmpFile);

	// reverse RGB to BGR 
	for (int j = 0; j < gnHeight; j++)
	{
		for (int i = 0; i < gnWidth; i++)
		{
			BYTE
				tmp = gpBufOut[(j * gnWidth + i) * 3 + 0];
				gpBufOut[(j * gnWidth + i) * 3 + 0] = gpBufOut[(j * gnWidth + i) * 3 + 2];
				gpBufOut[(j * gnWidth + i) * 3 + 2] = tmp;
		}
	}

	fwrite(gpBufOut, gnWidth * gnHeight * 3, 1, bmpFile);

	// Again, reverse BGR to RGB
	for (int j = 0; j < gnHeight; j++)
	{
		for (int i = 0; i < gnWidth; i++)
		{
			BYTE
				tmp = gpBufOut[(j * gnWidth + i) * 3 + 0];
				gpBufOut[(j * gnWidth + i) * 3 + 0] = gpBufOut[(j * gnWidth + i) * 3 + 2];
				gpBufOut[(j * gnWidth + i) * 3 + 2] = tmp;
		}
	}


	fclose(bmpFile);

	return true;
}

// Writes down the mask matrix from gpMask to the destination file
// Only 1 byte per pixel is used (no need to store RGB separately)
bool SaveMaskToBin(char *sFile)
{
	// Mask file

	FILE 
		*maskFile;
     if ((maskFile = fopen(sFile, "wb")) == NULL)
		return false;

	for (int j = 0; j < gnHeight; j++)
	{
		for (int i = 0; i < gnWidth; i++)
		{
			if (gpMask[j * gnWidth + i] == 0)
				gpMask[j * gnWidth + i] = 255;
			else
				gpMask[j * gnWidth + i] = 0;
		}
	}
	fwrite(gpMask, gnWidth * gnHeight, 1, maskFile);
	fclose(maskFile);

	return true;
}


// Writes down all pixels from gpBufOut to the destination
// jpg-file with name sFile
bool SaveResultToJPG(char* sFile)
{

	FILE 
		*outFile;
	
	outFile = fopen(sFile, "wb");
	if (outFile == NULL)
	{
		return false;
	}

	// Initialize jpeglib stuff
	jpeg_error_mgr 
		jerr;
	jpeg_compress_struct
		cinfo;
	cinfo.err = jpeg_std_error(&jerr);

	jpeg_create_compress(&cinfo);
	jpeg_stdio_dest(&cinfo, outFile);


	cinfo.image_width = gnWidth;
	cinfo.image_height = gnHeight;
	cinfo.input_components = 3;
	cinfo.in_color_space = JCS_RGB;
//	jpeg_set_colorspace(&cinfo, JCS_RGB);
	jpeg_set_defaults(&cinfo);

	JSAMPROW
		pRow[1];
	pRow[0] = gpBufIn;

	jpeg_set_quality(&cinfo, 97, TRUE);

	jpeg_start_compress(&cinfo, TRUE);


	// Actually write the image lines

	for (int j = 0; j < gnHeight; j++)
	{
		pRow[0] = gpBufOut + (gnWidth * 3) * j;
		jpeg_write_scanlines(&cinfo, pRow, 1);
	}

	jpeg_finish_compress(&cinfo);

	fclose(outFile);

	jpeg_destroy_compress(&cinfo);

	return true;
}

void CleanUp()
{
	SAFE_DELETE(gpBufIn);
	SAFE_DELETE(gpBufOut);
	SAFE_DELETE(gpMask);
}


